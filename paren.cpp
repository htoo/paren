// (C) 2013 Kim, Taegyoon
// The Paren Programming Language

#include <cstdio>
#include <cstring>
#include <string>
#include "libparen.h"
using namespace std;

int main(int argc, char *argv[]) {    
    if (argc <= 1) {
        libparen::paren p;
        p.print_logo();
        p.repl();
        puts("");
        return 0;
    }
    else if (argc == 2) {
        char *opt(argv[1]);
        if (strcmp(opt, "-h") == 0) {
            puts("Usage: paren [OPTIONS...] [FILES...]");
            puts("");
            puts("OPTIONS:");
            puts("    -h    print this screen.");
            puts("    -v    print version.");
            return 0;
        } else if (strcmp(opt, "-v") == 0) {
            puts(PAREN_VERSION);
            return 0;
        }
    }

    // execute files, one by one
    for (int i = 1; i < argc; i++) {
        libparen::paren p;
		string code;
		if (libparen::slurp(string(argv[i]), code)) {
			p.eval_string(code);
		} else {
            fprintf(stderr, "Cannot open file: %s\n", argv[i]);
        }
    }
}
