all: paren

paren: paren.cpp libparen.cpp libparen.h
	g++ -std=c++0x -s -Wall -O3 -static -o paren paren.cpp libparen.cpp

clean:
	rm -f paren paren.exe
